import Lottery from './models/Lottery'

try {
  const lottery = new Lottery(6, 4)
  lottery.generateTableGame()
  lottery.sortDozens()
  console.log(lottery.generateHtmlTable())
} 
catch (error) {
  console.log(error)  
}
