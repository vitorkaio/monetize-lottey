import { ErrorsMsgs } from '../msgs/Errors'

class Lottery {

  private qtdDozens: number // dezenas 6, 7, 8, 9 ou 10
  private result: Array<number> // resultado
  private gamesTotal: number // total de jogos
  private games: Array<Array<number>> // jogos


  /**
   * 3. Creates an instance of Lottery.
   * @param {number} qtdDozens
   * @param {number} gamesTotal
   * @memberof Lottery
   */
  constructor(qtdDozens: number, gamesTotal: number) {
    try {
      this.qtdDozens = this.verifyIntervalQtdDozens(qtdDozens)
      this.gamesTotal = gamesTotal
    } 
    catch (error) {
      throw(error)
    }
  }


  /**
   * 7. Desenvolver um método que confere todos os jogos e retorna uma tabela HTML
   * que contem os jogos e quantas dezenas foram sorteadas em cada jogo.
   *
   * @memberof Lottery
   */
  public generateHtmlTable(): string {
    const checks: Array<number> = new Array(this.getGames().length).fill(0)

    let tableHtml = `
    <!DOCTYPE html>
    <html>
      <head>
        <style>
          table, th, td {
            border: 1px solid black;
          }
          .print {
            color: red;
          }
        </style>
      </head>
      <body>
        <h4>Jogos</h4>
        <table>
    `
    for (let l: number = 0; l < this.getGames().length; l++) {
      tableHtml += '<tr>'
      for (let c: number = 0; c < this.getGames()[l].length; c++) {
        if (this.getResult().findIndex((number) => number === this.getGames()[l][c]) !== -1) {
          tableHtml += `<td class='print'>${this.getGames()[l][c]}</td>`
          checks[l] ++
        }
        else
          tableHtml += `<td>${this.getGames()[l][c]}</td>`
      }
      tableHtml += '</tr>'
    }

    tableHtml += '</table><h5><span>Resultado: </span>'

    for (let index: number = 0; index < this.getResult().length; index++) {
      tableHtml += `<span>${this.getResult()[index]} - </span>`
    }

    tableHtml += '</h5>'

    for (let index: number = 0; index < checks.length; index++) {
      tableHtml += `<h5> Jogo ${index + 1}: ${checks[index]} acertos </h5>`
    }

    tableHtml += `
    </body>
    </html>
    `
    return tableHtml
  }


  private checkValueInArray


  /**
   * 6. Desenvolver um método público que realize o sorteio de 6 dezenas aleatórias entre
   * 01 e 60. Os números não podem se repetir e devem estar em ordem crescente. O
   * array resultante deverá ser armazenado no atributo “Resultado”.
   *
   * @memberof Lottery
   */
  public sortDozens(): void {
    const game: Array<number> = this.generateOneGame()
    if (game.length > 6) {
      game.splice(6, game.length)
    }
    this.setResult(game)
  }
  
  /**
   * 5. Desenvolver um método público que selecione a quantidade de jogos que está
   * setado no atributo “Total jogos” obtendo assim um array multidimensional onde cada
   * posição deste array deverá conter outro array com um jogo. Use o método anterior
   * para gerar cada jogo e salve este array multidimensional no atributo “Jogos”.
   *
   * @memberof Lottery
   */
  public generateTableGame(): void {
    const tableGame: Array<Array<number>> = []
    for (let cont: number = 0; cont < this.getGamesTotal(); cont++) {
      tableGame.push(this.generateOneGame())
    }
    this.setGames(tableGame)
  }

  /**
   * 4. Desenvolver um método privado que retorne um array com dezenas entre 01 e 60
   * que respeite a cardinalidade definida pelo atributo “Quantidade de dezenas” onde as
   * dezenas nunca se repitam e estejam na ordem crescente.
   *
   * @returns {Array<number>}
   * @memberof Lottery
   */
  private generateOneGame(): Array <number> {
    const numbers: Set<number> = new Set;
    while (numbers.size < this.getQtdDozens()) 
      numbers.add(Math.floor(Math.random() * 60) + 1);
    
    return [...numbers].sort((a, b) => a - b)
  }

  /**
   * Verifica o intervalo de qtdDozens. Caso esteja fora do intervalo entre 6 e 10 uma exceptions é lançada.
   * caso contrário o valor é retornado.
   * @private
   * @param {number} qtdDozens
   * @memberof Lottery
   */
  private verifyIntervalQtdDozens(qtdDozens: number): number | never {
    if (qtdDozens < 6 || qtdDozens > 10)
      throw(ErrorsMsgs.DEZENS_INTERVAL)
    else
      return qtdDozens
  }


  // qtdDozens
  public setQtdDozens(qtdDozens: number): void {
    this.qtdDozens = qtdDozens
  }

  public getQtdDozens(): number {
    return this.qtdDozens
  }

  // result
  public setResult(result: Array<number>): void {
    this.result = result
  }

  public getResult(): Array<number> {
    return this.result
  }

  // gamesTotal
  public setGamesTotal(gamesTotal: number): void {
    this.gamesTotal = gamesTotal
  }

  public getGamesTotal(): number {
    return this.gamesTotal
  }

  // games
  public setGames(games: Array<Array<number>>): void {
    this.games = games
  }

  public getGames(): Array<Array<number>> {
    return this.games
  }




}

export default Lottery
